import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Exercice2Page } from './exercice2.page';

const routes: Routes = [
  {
    path: '',
    component: Exercice2Page
  },
  {
    path: 'ex2-modal',
    loadChildren: () => import('./ex2-modal/ex2-modal.module').then( m => m.Ex2ModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Exercice2PageRoutingModule {}
