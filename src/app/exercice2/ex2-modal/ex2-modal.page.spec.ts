import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ex2ModalPage } from './ex2-modal.page';

describe('Ex2ModalPage', () => {
  let component: Ex2ModalPage;
  let fixture: ComponentFixture<Ex2ModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ex2ModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ex2ModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
