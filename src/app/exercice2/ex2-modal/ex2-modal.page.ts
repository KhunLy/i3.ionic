import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PokemonDetailsRequest } from 'src/app/_models/pokemon-details-request';
import { PokemonService } from 'src/app/_services/pokemon.service';
import { RadialChartOptions, ChartDataSets, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-ex2-modal',
  templateUrl: './ex2-modal.page.html',
  styleUrls: ['./ex2-modal.page.scss'],
})
export class Ex2ModalPage implements OnInit {

  @Input() url: string;

  model: PokemonDetailsRequest

  radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  radarChartLabels: Label[] = [];

  radarChartData: ChartDataSets[] = [
    { data: [], label: '' },
  ];
  radarChartType: ChartType = 'radar';

  constructor(
    private modalCtrl: ModalController,
    private pokemonService: PokemonService
  ) { }

  ngOnInit() {
    this.pokemonService
      .getDetails(this.url)
      .then(data => {
        this.model = data;
        this.radarChartLabels = data.stats.map(s => s.stat.name);
        this.radarChartData[0].data = data.stats.map(s => s.base_stat);
      });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
