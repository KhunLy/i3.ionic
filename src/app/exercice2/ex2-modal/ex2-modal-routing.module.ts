import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ex2ModalPage } from './ex2-modal.page';

const routes: Routes = [
  {
    path: '',
    component: Ex2ModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ex2ModalPageRoutingModule {}
