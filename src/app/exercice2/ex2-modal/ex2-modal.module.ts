import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ex2ModalPageRoutingModule } from './ex2-modal-routing.module';

import { Ex2ModalPage } from './ex2-modal.page';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ex2ModalPageRoutingModule,
  ],
  declarations: [Ex2ModalPage]
})
export class Ex2ModalPageModule {}
