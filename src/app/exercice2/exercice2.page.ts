import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../_services/pokemon.service';
import { PokemonsRequest } from '../_models/pokemons-request';
import { ModalController } from '@ionic/angular';
import { Ex2ModalPage } from './ex2-modal/ex2-modal.page';

@Component({
  selector: 'app-exercice2',
  templateUrl: './exercice2.page.html',
  styleUrls: ['./exercice2.page.scss'],
})
export class Exercice2Page implements OnInit {

  model: PokemonsRequest;
  constructor(
    private pokemonService: PokemonService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.pokemonService
      .getAll('https://pokeapi.co/api/v2/pokemon')
      .then(data => this.model = data);
  }

  goPrev() {
    this.pokemonService
      .getAll(this.model.previous)
      .then(data => this.model = data);
  }

  goNext() {
    this.pokemonService
      .getAll(this.model.next)
      .then(data => this.model = data);
  }

  openModal(url) {
    this.modalCtrl.create({
      component: Ex2ModalPage,
      componentProps: {
        url: url
      }
    }).then(modal => modal.present())
  }

}
