import { Component, OnInit } from '@angular/core';
import { Article } from '../_models/article';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-exercice1',
  templateUrl: './exercice1.page.html',
  styleUrls: ['./exercice1.page.scss'],
})
export class Exercice1Page implements OnInit {

  item: string;

  listItems: Article[]; //Array<string>;

  constructor(
    private alertController: AlertController,
    private storage: Storage
  ) { 
  }

  ngOnInit() {
    this.storage.get('Articles').then(data => {
      this.listItems = data || []
    });
    //this.listItems = [];
  }

  add() {
    this.listItems.push({ name: this.item, isChecked: false });
    this.item = null;
    this.save();
  }

  cocher(elem: Article) {
    elem.isChecked = !elem.isChecked;
    this.save();
  }

  delete(elem: Article) {
    let index = this.listItems.indexOf(elem);
    this.listItems.splice(index, 1);
    this.save();
  }

  showAlert(elem: Article){
    this.alertController.create({
      header: 'Attention',
      subHeader: 'Êtes-vous sûr de vouloir supprimer l\'article ' 
       + elem.name,
      buttons: [{ 
        text: 'oui',
        handler: () => this.delete(elem)
      },{ 
        text: 'non', 
      }]
    }).then(alert => alert.present());
  }

  private save() {
    this.storage.set('Articles', this.listItems);
  }

}
