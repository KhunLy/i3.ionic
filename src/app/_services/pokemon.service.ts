import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokemonsRequest } from '../_models/pokemons-request';
import { PokemonDetailsRequest } from '../_models/pokemon-details-request';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(url: string) {
    return this.httpClient.get<PokemonsRequest>(url)
      .toPromise();
  }

  getDetails(url: string) {
    return this.httpClient
      .get<PokemonDetailsRequest>(url)
      .toPromise();
  }
}
