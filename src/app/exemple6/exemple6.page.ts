import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../_services/weather.service';
import { WeatherData } from '../_models/weather';

@Component({
  selector: 'app-exemple6',
  templateUrl: './exemple6.page.html',
  styleUrls: ['./exemple6.page.scss'],
})
export class Exemple6Page implements OnInit {

  constructor(
    private weatherService: WeatherService
  ) { }

  cityName: string;

  weather: WeatherData;

  ngOnInit() {
  }

  search() {
    this.weatherService.getWeather(this.cityName)
      .then(data => this.weather = data);
  }



}
