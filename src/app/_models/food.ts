export interface FoodData {
  status: number;
  product: Product;
  code: string;
  status_verbose: string;
}

interface Product {
  ingredients_ids_debug: string[];
  nutriscore_score: number;
  selected_images: Selectedimages;
  labels_tags: any[];
  no_nutrition_data: string;
  _id: string;
  product_quantity: string;
  nova_group_debug: string;
  nutrition_data: string;
  categories_hierarchy: string[];
  data_quality_bugs_tags: any[];
  additives_n: number;
  packaging_tags: any[];
  ingredients_n_tags: string[];
  completeness: number;
  last_image_dates_tags: string[];
  created_t: number;
  carbon_footprint_from_known_ingredients_debug: string;
  allergens_tags: string[];
  allergens: string;
  entry_dates_tags: string[];
  pnns_groups_2_tags: string[];
  languages_codes: Languagescodes;
  nutrition_score_beverage: number;
  origins_tags: any[];
  creator: string;
  interface_version_modified: string;
  cities_tags: any[];
  serving_size: string;
  last_editor?: any;
  nutrition_score_warning_no_fruits_vegetables_nuts: number;
  emb_codes_tags: any[];
  image_ingredients_small_url: string;
  categories: string;
  ingredients_text_debug: string;
  nucleotides_tags: any[];
  unknown_nutrients_tags: any[];
  allergens_from_ingredients: string;
  image_nutrition_small_url: string;
  countries_hierarchy: string[];
  labels_lc: string;
  stores_tags: string[];
  last_edit_dates_tags: string[];
  nutrient_levels: Nutrientlevels;
  carbon_footprint_percent_of_known_ingredients: number;
  lc: string;
  labels: string;
  brands: string;
  states_hierarchy: string[];
  allergens_hierarchy: string[];
  last_modified_t: number;
  generic_name: string;
  last_image_t: number;
  amino_acids_prev_tags: any[];
  product_name_debug_tags: any[];
  languages_tags: string[];
  pnns_groups_1: string;
  photographers_tags: string[];
  additives_old_tags: string[];
  purchase_places_tags: string[];
  nutriscore_data: Nutriscoredata;
  pnns_groups_1_tags: string[];
  data_quality_errors_tags: any[];
  additives_original_tags: string[];
  nutrition_data_prepared_per: string;
  ingredients_text_with_allergens_fr: string;
  id: string;
  image_nutrition_thumb_url: string;
  ingredients_debug: (null | string)[];
  nutrition_grades: string;
  editors_tags: string[];
  countries_lc: string;
  languages: Languages;
  nutrition_data_prepared: string;
  ingredients_from_or_that_may_be_from_palm_oil_n: number;
  traces: string;
  popularity_tags: string[];
  debug_param_sorted_langs: string[];
  link: string;
  vitamins_tags: any[];
  nutrition_grade_fr: string;
  update_key: string;
  correctors_tags: string[];
  data_quality_info_tags: string[];
  images: Images;
  nova_groups_tags: string[];
  emb_codes: string;
  nutrition_score_warning_no_fiber: number;
  product_name_fr: string;
  ingredients_n: number;
  data_quality_warnings_tags: string[];
  interface_version_created: string;
  last_modified_by?: any;
  origins: string;
  traces_lc: string;
  compared_to_category: string;
  nova_group_tags: string[];
  manufacturing_places: string;
  additives_tags: string[];
  max_imgid: string;
  stores: string;
  states: string;
  checkers_tags: any[];
  ingredients_that_may_be_from_palm_oil_n: number;
  product_name: string;
  traces_tags: string[];
  ingredients_that_may_be_from_palm_oil_tags: any[];
  serving_quantity: string;
  'fruits-vegetables-nuts_100g_estimate': number;
  additives_prev_original_tags: string[];
  image_nutrition_url: string;
  quantity: string;
  languages_hierarchy: string[];
  nova_group: number;
  sortkey: number;
  additives_debug_tags: any[];
  code: string;
  ingredients_analysis_tags: string[];
  ingredients_text_debug_tags: any[];
  brands_tags: string[];
  image_ingredients_thumb_url: string;
  nova_groups: string;
  allergens_lc: string;
  image_ingredients_url: string;
  ingredients_original_tags: string[];
  allergens_from_user: string;
  nutrition_score_debug: string;
  nucleotides_prev_tags: any[];
  nutrition_data_per: string;
  states_tags: string[];
  _keywords: string[];
  misc_tags: string[];
  minerals_tags: any[];
  additives_old_n: number;
  traces_from_user: string;
  packaging: string;
  generic_name_fr: string;
  manufacturing_places_tags: any[];
  ingredients_text_fr: string;
  pnns_groups_2: string;
  ingredients_text: string;
  nutrition_grades_tags: string[];
  unknown_ingredients_n: number;
  unique_scans_n: number;
  categories_tags: string[];
  expiration_date: string;
  rev: number;
  traces_hierarchy: string[];
  countries: string;
  countries_tags: string[];
  ingredients_text_with_allergens: string;
  categories_lc: string;
  purchase_places: string;
  ingredients_from_palm_oil_n: number;
  nutriments: Nutriments;
  nutrient_levels_tags: string[];
  traces_from_ingredients: string;
  data_quality_tags: string[];
  ingredients_from_palm_oil_tags: string[];
  other_nutritional_substances_tags: any[];
  informers_tags: string[];
  lang: string;
  labels_hierarchy: any[];
  minerals_prev_tags: any[];
  scans_n: number;
  ingredients_tags: string[];
  vitamins_prev_tags: any[];
  amino_acids_tags: any[];
  nutriscore_grade: string;
  complete: number;
  ingredients_hierarchy: string[];
  ingredients: Ingredient[];
  codes_tags: string[];
}

interface Ingredient {
  rank?: number;
  percent?: string;
  id: string;
  has_sub_ingredients?: string;
  text: string;
  vegan?: string;
  vegetarian?: string;
  from_palm_oil?: string;
}

interface Nutriments {
  sodium_serving: number;
  energy_prepared_value: number;
  'saturated-fat_prepared_value': number;
  'saturated-fat_value': number;
  'energy-kcal': number;
  sugars_serving: number;
  'nova-group': number;
  salt_100g: number;
  sugars_unit: string;
  sugars: number;
  'energy-kcal_100g': number;
  'energy-kcal_serving': number;
  sodium_unit: string;
  'saturated-fat_prepared_serving': number;
  fat_prepared_100g: number;
  'carbon-footprint-from-known-ingredients_product': number;
  'energy-kcal_value': number;
  'energy-kcal_prepared': number;
  'energy-kj': number;
  energy_serving: number;
  sugars_value: number;
  'nova-group_100g': number;
  fat_prepared_unit: string;
  proteins_prepared_unit: string;
  energy_value: number;
  'energy-kj_serving': number;
  energy_100g: number;
  fat_prepared_serving: number;
  'energy-kj_prepared': number;
  'nutrition-score-fr_100g': number;
  salt_unit: string;
  fat_value: number;
  carbohydrates_100g: number;
  sugars_prepared_100g: number;
  sodium_prepared_serving: number;
  'saturated-fat_100g': number;
  sugars_prepared_value: number;
  'energy-kj_prepared_value': number;
  proteins_prepared_serving: number;
  'saturated-fat_prepared_100g': number;
  sodium_prepared_unit: string;
  salt_prepared: number;
  carbohydrates_unit: string;
  carbohydrates_prepared_unit: string;
  'nutrition-score-uk_100g': number;
  fat_unit: string;
  fat_prepared: number;
  carbohydrates_prepared_100g: number;
  'energy-kj_prepared_100g': number;
  proteins_prepared_100g: number;
  'energy-kcal_prepared_100g': number;
  energy: number;
  'nova-group_serving': number;
  proteins: number;
  proteins_serving: number;
  'saturated-fat_unit': string;
  salt_value: number;
  'energy-kj_unit': string;
  carbohydrates_serving: number;
  'energy-kcal_prepared_value': number;
  'saturated-fat': number;
  carbohydrates: number;
  'energy-kcal_prepared_serving': number;
  salt_prepared_value: number;
  'carbon-footprint-from-known-ingredients_100g': number;
  fat_100g: number;
  energy_prepared: number;
  'saturated-fat_prepared_unit': string;
  proteins_100g: number;
  fat_serving: number;
  sugars_prepared: number;
  energy_unit: string;
  sodium_prepared_value: number;
  sodium_100g: number;
  carbohydrates_prepared_serving: number;
  'energy-kj_prepared_unit': string;
  'energy-kcal_unit': string;
  proteins_unit: string;
  energy_prepared_serving: number;
  sodium_prepared_100g: number;
  salt_prepared_unit: string;
  proteins_value: number;
  carbohydrates_prepared_value: number;
  fat: number;
  energy_prepared_unit: string;
  salt: number;
  sugars_prepared_serving: number;
  'nutrition-score-fr': number;
  'energy-kj_prepared_serving': number;
  'saturated-fat_prepared': number;
  sugars_prepared_unit: string;
  proteins_prepared: number;
  fat_prepared_value: number;
  carbohydrates_value: number;
  'energy-kcal_prepared_unit': string;
  carbohydrates_prepared: number;
  proteins_prepared_value: number;
  'carbon-footprint-from-known-ingredients_serving': number;
  sodium_prepared: number;
  sodium_value: number;
  'nutrition-score-uk': number;
  salt_serving: number;
  sodium: number;
  energy_prepared_100g: number;
  salt_prepared_100g: number;
  'saturated-fat_serving': number;
  'energy-kj_100g': number;
  salt_prepared_serving: number;
  'energy-kj_value': number;
  sugars_100g: number;
}

interface Images {
  '1': _1;
  '2': _1;
  '3': _3;
  ingredients_fr: Ingredientsfr;
  front_nl: Frontnl;
  nutrition_fr: Nutritionfr;
}

interface Nutritionfr {
  x2?: any;
  angle?: any;
  geometry: string;
  sizes: Sizes2;
  y1?: any;
  orientation: string;
  rev: string;
  y2?: any;
  x1?: any;
  imgid: string;
  normalize: string;
  ocr: number;
  white_magic: string;
}

interface Frontnl {
  white_magic?: any;
  normalize?: any;
  x1: string;
  imgid: string;
  rev: string;
  y2: string;
  y1: string;
  sizes: Sizes2;
  geometry: string;
  angle: number;
  x2: string;
}

interface Ingredientsfr {
  angle?: any;
  x2?: any;
  x1?: any;
  imgid: string;
  normalize: string;
  ocr: number;
  white_magic: string;
  orientation?: any;
  rev: string;
  y2?: any;
  y1?: any;
  sizes: Sizes2;
  geometry: string;
}

interface Sizes2 {
  '100': _100;
  '200': _100;
  '400': _100;
  full: _100;
}

interface _3 {
  sizes: Sizes;
  uploader: string;
  uploaded_t: number;
}

interface _1 {
  sizes: Sizes;
  uploader: string;
  uploaded_t: string;
}

interface Sizes {
  '100': _100;
  '400': _100;
  full: _100;
}

interface _100 {
  w: number;
  h: number;
}

interface Languages {
  'en:french': number;
  'en:dutch': number;
}

interface Nutriscoredata {
  fiber: number;
  saturated_fat_ratio_points: number;
  sodium_value: number;
  saturated_fat_points: number;
  saturated_fat: number;
  negative_points: number;
  energy: number;
  proteins: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils: number;
  energy_points: number;
  saturated_fat_ratio_value: number;
  positive_points: number;
  fiber_value: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_value: number;
  sodium: number;
  saturated_fat_ratio: number;
  is_cheese: number;
  score: number;
  proteins_points: number;
  is_beverage: number;
  sugars: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_points: number;
  sodium_points: number;
  sugars_value: number;
  grade: string;
  fiber_points: number;
  saturated_fat_value: number;
  energy_value: number;
  proteins_value: number;
  is_fat: number;
  sugars_points: number;
  is_water: number;
}

interface Nutrientlevels {
  'saturated-fat': string;
  sugars: string;
  salt: string;
  fat: string;
}

interface Languagescodes {
  nl: number;
  fr: number;
}

interface Selectedimages {
  front: Front;
  nutrition: Nutrition;
  ingredients: Nutrition;
}

interface Nutrition {
  display: Display;
  small: Display;
  thumb: Display;
}

interface Display {
  fr: string;
}

interface Front {
  small: Small;
  display: Small;
  thumb: Small;
}

interface Small {
  nl: string;
}