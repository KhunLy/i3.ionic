import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Binding',
      icon: 'book',
      url: '/exemple1'
    },
    {
      title: 'Event',
      icon: 'flash',
      url: '/exemple2'
    },
    {
      title: '*ngIf',
      icon: 'heart',
      url: '/exemple3'
    },
    {
      title: '*ngFor',
      icon: 'beer',
      url: '/exemple4'
    },
    {
      title: 'Binding 2 Ways',
      icon: 'wine',
      url: '/exemple5'
    },
    {
      title: 'Meteo',
      icon: 'sunny',
      url: '/exemple6'
    },
    {
      title: 'Camera',
      icon: 'barcode',
      url: '/camera'
    },
    {
      title: 'Exercice 1',
      icon: 'cart',
      url: '/exercice1'
    },
    {
      title: 'Exercice 2',
      icon: 'bug',
      url: '/exercice2'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
